# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class FalvtiaowenItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    url = scrapy.Field()
    title = scrapy.Field()
    content = scrapy.Field()
    city = scrapy.Field()
    organization = scrapy.Field()
    law_num = scrapy.Field()
    pub_date = scrapy.Field()
    exec_date = scrapy.Field()
    effect_date = scrapy.Field()
    effect_level = scrapy.Field()
