# -*- coding: utf-8 -*-
import scrapy
from urllib import parse
from falvtiaowen.items import FalvtiaowenItem
from scrapy.utils.response import get_base_url


class FindlawSpider(scrapy.Spider):
    name = 'findlaw'
    allowed_domains = ['china.findlaw.cn']
    start_urls = ['http://china.findlaw.cn/fagui/area/beijing/p_1/']

    def __init__(self, *args, **kwargs):
        super(FindlawSpider, self).__init__(*args, **kwargs)
        # 北京、天津、上海、重庆、台湾、香港、澳门
        self.directly_city_dict = {
            '北京': 'http://china.findlaw.cn/fagui/area/beijing/p_1/',
            '天津': 'http://china.findlaw.cn/fagui/area/tianjin/p_1/',
            '上海': 'http://china.findlaw.cn/fagui/area/shanghai/p_1/',
            '重庆': 'http://china.findlaw.cn/fagui/area/chongqing/p_1/',
            '台湾省': 'http://china.findlaw.cn/fagui/area/taiwan/p_1/',
            '香港特别行政区': 'http://china.findlaw.cn/fagui/area/xianggang/p_1/',
            '澳门特别行政区': 'http://china.findlaw.cn/fagui/area/aomen/p_1/'
        }

    def get_compurl(self, broken_url, response):
        """
        # 构造完整的url
        :param broken_url: 页面获取到的url(可能是相对的，也可能是绝对的)
        :param response: 当前页面的内容
        :return: 完整的url
        """
        base_url = get_base_url(response)
        complete_url = parse.urljoin(base_url, broken_url)
        return complete_url

    def parse(self, response):
        """
        :param response: 回调返回来的response
        :return: 返回request对象，url为解析到的城市详情页的链接
        """
        city_url_list = response.xpath('//div[@class="aside-menu-lit"]//div[@class="aside-menu-three-li"]//a/@href').extract()
        city = response.xpath('//div[@class="aside-menu-lit"]//div[@class="aside-menu-three-li"]//a/text()').extract()
        # 将城市名字和url一块打包 进行遍历
        for city_url, city in zip(city_url_list, city):
            city_url = self.get_compurl(city_url, response)
            yield scrapy.Request(city_url, callback=self.parseurl, meta={'city': city})
        for name, url in self.directly_city_dict.items():
            yield scrapy.Request(url, callback=self.parseurl, meta={'city': name})

    def parseurl(self, response):
        """
        # 解析列表页的url 和 下一页连接
        :param response: 回调返回来的response
        :return: 返回request对象
        """
        detail_url_list = response.xpath('//ul[@class="aside-info-listbox-ul"]//a/@href').extract()
        for detail_url in detail_url_list:
            detail_url = self.get_compurl(detail_url, response)
            yield scrapy.Request(detail_url, callback=self.parsedetail, meta={'url': detail_url,
                                                                              'city': response.meta['city']})
        next_page_node_list = response.xpath('//div[@class="Paging"]//a')
        for next_page_node in next_page_node_list:
            if '下一页' in next_page_node.xpath('text()').extract_first():
                next_page_url = self.get_compurl(next_page_node.xpath('@href').extract_first(), response)
                yield scrapy.Request(next_page_url, callback=self.parseurl, meta={'city': response.meta['city']})

    def parsedetail(self, response):
        """
        # 解析详情页 获取想要的字段内容
        :param response: 回调返回来的response
        :return:  将填充好的item传到pipelines继续处理
        """
        item = FalvtiaowenItem()
        item['url'] = response.meta['url']
        if '香港' not in response.meta['city'] and '澳门' not in response.meta['city'] and '台湾' not in response.meta['city']:
            item['city'] = response.meta['city'] + '市'
        else:
            item['city'] = response.meta['city']
        item['title'] = response.xpath('//div[@class="artcontent"]//h1[@class="art-h1"]/text()').extract_first().strip()
        item['content'] = response.xpath('//div[@class="art-info"]').extract_first().strip()
        item['organization'] = response.xpath('//div[@class="art-info-table"]//a/text()').extract_first()
        if item['organization']:
            item['organization'] = item['organization'].strip()
        info_list = response.xpath('//div[@class="art-info-table"]//td/text()').extract()
        info_dict = dict([i.strip().split('：') for i in info_list if i])
        item['law_num'] = info_dict.setdefault('文号', None)
        item['pub_date'] = info_dict.setdefault('颁布日期', None)
        item['exec_date'] = info_dict.setdefault('执行日期', None)
        item['effect_date'] = info_dict.setdefault('时 效 性', None)
        item['effect_level'] = info_dict.setdefault('效力级别', None)

        yield item
