# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/spider-middleware.html

import os, random, logging, base64, hashlib

import redis, requests, json
from scrapy import signals
from scrapy.dupefilters import RFPDupeFilter
from scrapy.utils.url import canonicalize_url


class FalvtiaowenSpiderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Response, dict
        # or Item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class RandomUserAgentMiddleware(object):
    def __init__(self, agents):
        self.agents = agents

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings.getlist('USER_AGENTS'))

    def process_request(self, request, spider):
        request.headers.setdefault('User-Agent', random.choice(self.agents))


class ProxyMiddleware(object):
    def __init__(self):
        # 阿布云代理服务器
        self.proxyServer = "http://http-dyn.abuyun.com:9020"
        # 代理隧道验证信息
        proxyUser = "HBJ5L993OE214T5D"
        proxyPass = "32A88E570E701353"
        self.proxyAuth = "Basic " + base64.urlsafe_b64encode(bytes((proxyUser + ":" + proxyPass), "ascii")).decode("utf8") # Python3

    def process_request(self, request, spider):
        '''处理请求request'''
        request.headers['Proxy-Authorization'] = self.proxyAuth
        request.meta['proxy'] = self.proxyServer

    # 芝麻代理
    # def process_request(self, request, spider):
    #     print('正在请求代理')
    #     res = requests.get('http://webapi.http.zhimacangku.com/getip?num=1&type=2&pro=0&city=0&yys=0&port=1&pack=15433&ts=0&ys=0&cs=0&lb=1&sb=0&pb=4&mr=2&regions=', timeout=5)
    #     if res.status_code == 200:
    #         proxy_dict = json.loads(res.text).get('data')[0]
    #         request.meta['proxy'] = 'http://{}:{}'.format(proxy_dict.get('ip'), proxy_dict.get('port'))
    #         print('请求到的代理为:\t{}'.format(request.meta['proxy']))
    #     else:
    #         print('代理请求出错,使用本机ip进行访问!')

    def process_response(self, request, response, spider):
        '''处理返回的response'''
        print('正在处理返回结果,url:{}'.format(response.url))
        if response.status == 200:
            return response
        else:
            print('页面出现没有考虑到的状态吗,正在重新请求...')
            new_request = request.copy()
            new_request.dont_filter = True
            return new_request

    def process_exception(self, request, exception, spider):
        new_request = request.copy()
        new_request.dont_filter = True
        return new_request


# 实现url的去重
class URLRedisFilter(RFPDupeFilter):
    # """ 只根据url去重"""
    def __init__(self, path=None, debug=False):
        RFPDupeFilter.__init__(self, path, debug)
        self.redis_key = 'spider_already_url'
        self.redis_db = 0
        self.dupefilter = UrlFilterAndAdd(redis_key=self.redis_key, redis_db=self.redis_db)

    def request_seen(self, request):
        # 校验，新增2行代码
        if self.dupefilter.check_url(request.url):
            print('我在redis去重里面,此url{}已经被请求过了'.format(request.url))
            return True
        # 保留中间页面的去重规则不变，不然爬虫在运行过程中容易出现死循环
        fp = self.request_fingerprint(request)
        if fp in self.fingerprints:
            print('我在指纹去重里面,此url{}已经被请求过了'.format(request.url))
            return True
        self.fingerprints.add(fp)
        if self.file:
            self.file.write(fp + os.linesep)
        return False

# 操作redis
class UrlFilterAndAdd(object):
    def __init__(self, redis_key, redis_db):
        # 这里的ip 和端口 要改的
        self.host = '192.168.200.166'
        self.port = 6399
        self.db = redis_db
        self.pool = redis.ConnectionPool(host=self.host, port=self.port, db=self.db)
        self.redis = redis.StrictRedis(connection_pool=self.pool)
        self.key = redis_key

    def url_sha1(self, url):
        fp = hashlib.sha1()
        fp.update(canonicalize_url(url).encode("utf-8"))
        url_sha1 = fp.hexdigest()
        return url_sha1

    def check_url(self, url):
        sha1 = self.url_sha1(url)
        # 此处只判断url是否在set中，并不添加url信息，
        # 防止将起始url 、中间url(比如列表页的url地址)写入缓存中，
        isExist = self.redis.sismember(self.key, sha1)
        return isExist

    def add_url(self, url):
        sha1 = self.url_sha1(url)
        added = self.redis.sadd(self.key, sha1)
        self.redis.incr(self.key+'_count')
        return added