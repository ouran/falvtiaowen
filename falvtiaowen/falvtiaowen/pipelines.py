# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymysql
from falvtiaowen.settings import host, username, password, port, charset, db


class FalvtiaowenPipeline(object):
    def __init__(self):
        self.client = pymysql.connect(host=host, user=username, password=password, port=port, db=db, charset=charset)
        self.cur = self.client.cursor()

    def process_item(self, item, spider):
        # 先将item 入道文章表里面
        fetch_sql = """
            select id from falvtiaowen_articles where title='%s' 
        """%(item.get('title'))
        self.cur.execute(fetch_sql)
        result = self.cur.fetchone()
        if not result:
            article_sql = """
                insert into falvtiaowen_articles(url, title, content, organization, law_num,
                pub_date, exec_date, effect_date, effect_level)
                values('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
            """ % (item.get('url'), item.get('title'), item.get('content'), item.get('organization'), item.get('law_num'),
                   item.get('pub_date'), item.get('exec_date'), item.get('effect_date'), item.get('effect_level'),)
            self.cur.execute(article_sql)
            self.client.commit()
            # 获取文章id
            article_id = self.cur.lastrowid

            # 获取法律效力级别的id
            effect_sql = """
                select id from efficacy_level where efficacy_level='%s'
            """ % (item.get('effect_level'))
            self.cur.execute(effect_sql)
            effect_id = self.cur.fetchone()
            if effect_id:
                effect_id = effect_id[0]

            # 获取城市id
            city_sql = """
                select id from area where `name`='%s'
            """ % (item.get('city'))
            self.cur.execute(city_sql)
            city_id = self.cur.fetchone()[0]

            repl_sql = """
                insert into falvtiaowen_repl(article_id, city_id, effect_level_id)
                values(%s, %s, %s)
            """ % (article_id, city_id, effect_id)
            self.cur.execute(repl_sql)
            self.client.commit()

        return item
